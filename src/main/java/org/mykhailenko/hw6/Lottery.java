package org.mykhailenko.hw6;

import java.util.*;

public class Lottery {
    public static ArrayList<Integer> generateLotteryNumbers(int min, int max) {
        Random random = new Random();
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        while (numbers.size() < 6) {
            int randomNumber = random.nextInt(max) + min;

            if (!numbers.contains(randomNumber)) {
                numbers.add(randomNumber);
            }
        }
        return numbers;
    }

    public static void main(String[] args) {
        System.out.println("Your lucky lottery numbers: " + generateLotteryNumbers(1, 36));
    }
}
