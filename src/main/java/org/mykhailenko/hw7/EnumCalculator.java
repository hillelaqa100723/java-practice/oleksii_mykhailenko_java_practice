package org.mykhailenko.hw7;

import java.util.ArrayList;
import java.util.List;

public class EnumCalculator {

    enum Operation {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        REMAINDER("%");

        private String value;

        Operation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static Double calculate(List<String> data) {
        Double result;
        String operationString = data.get(1);
        Operation operation = Operation.valueOf(operationString.toUpperCase());
        Double firstNumber = Double.parseDouble(data.get(0));
        Double secondNumber = Double.parseDouble(data.get(2));

        switch (operation) {
            case PLUS:
                result = firstNumber + secondNumber;
                break;
            case MINUS:
                result = firstNumber - secondNumber;
                break;
            case MULTIPLY:
                result = firstNumber * secondNumber;
                break;
            case DIVIDE:
                if (secondNumber != 0) {
                    result = firstNumber / secondNumber;
                } else {
                    System.out.println("You can't divide by zero!");
                    return null;
                }
                break;
            case REMAINDER:
                if (secondNumber != 0) {
                    result = firstNumber % secondNumber;
                } else {
                    System.out.println("You can't divide by zero!");
                    return null;
                }
                break;
            default:
                return null;
        }
        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        String resultString;
        String operationString = data.get(1);
        Operation operation = Operation.valueOf(operationString.toUpperCase());
        Double firstNumber = Double.parseDouble(data.get(0));
        Double secondNumber = Double.parseDouble(data.get(2));

        switch (operation) {
            case PLUS:
                Operation operationPlus = Operation.PLUS;
                resultString = firstNumber + operationPlus.getValue() + secondNumber + "=" + result;
                break;
            case MINUS:
                Operation operationMinus = Operation.MINUS;
                resultString = firstNumber + operationMinus.getValue() + secondNumber + "=" + result;
                break;
            case MULTIPLY:
                Operation operationMultiply = Operation.MULTIPLY;
                resultString = firstNumber + operationMultiply.getValue() + secondNumber + "=" + result;
                break;
            case DIVIDE:
                Operation operationDivide = Operation.DIVIDE;
                if (secondNumber != 0) {
                    resultString = firstNumber + operationDivide.getValue() + secondNumber + "=" + result;
                } else {
                    resultString = firstNumber + operationDivide.getValue() + secondNumber + "=" + null;
                }
                break;
            case REMAINDER:
                Operation operationRemainder = Operation.REMAINDER;
                if (secondNumber != 0) {
                    resultString = firstNumber + operationRemainder.getValue() + secondNumber + "=" + result;
                } else {
                    resultString = firstNumber + operationRemainder.getValue() + secondNumber + "=" + null;
                }
                break;
            default:
                return null;
        }
        return resultString;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("25");
        list.add("reMAINder");
        list.add("12");
        System.out.println(prepareResultString(list, calculate(list)));
    }
}
