package org.mykhailenko.lesson5_hw2;

public class SumOfDimensionalArrays {

    public static int[] sumInRows(int[][] source) {
        int[] sum = new int[source.length];

        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[i].length; j++) {
                sum[i] += source[i][j];
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int[][] newArr = new int[][] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[] result = sumInRows(newArr);
        System.out.print("Sum elements in every rows is: ");
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
    }
}
