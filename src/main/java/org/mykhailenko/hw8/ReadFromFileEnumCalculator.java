package org.mykhailenko.hw8;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class ReadFromFileEnumCalculator {

    enum Operation {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        REMAINDER("%");

        private String value;

        Operation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static Double calculate(List<String> data) {
        Double result;
        String operationString = data.get(1);
        Operation operation = Operation.valueOf(operationString.toUpperCase());
        Double firstNumber = Double.parseDouble(data.get(0));
        Double secondNumber = Double.parseDouble(data.get(2));

        switch (operation) {
            case PLUS:
                result = firstNumber + secondNumber;
                break;
            case MINUS:
                result = firstNumber - secondNumber;
                break;
            case MULTIPLY:
                result = firstNumber * secondNumber;
                break;
            case DIVIDE:
                if (secondNumber != 0) {
                    result = firstNumber / secondNumber;
                } else {
                    System.out.println("You can't divide by zero!");
                    return null;
                }
                break;
            case REMAINDER:
                if (secondNumber != 0) {
                    result = firstNumber % secondNumber;
                } else {
                    System.out.println("You can't divide by zero!");
                    return null;
                }
                break;
            default:
                return null;
        }
        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        String resultString;
        String operationString = data.get(1);
        Operation operation = Operation.valueOf(operationString.toUpperCase());
        Double firstNumber = Double.parseDouble(data.get(0));
        Double secondNumber = Double.parseDouble(data.get(2));

        switch (operation) {
            case PLUS:
                Operation operationPlus = Operation.PLUS;
                resultString = firstNumber + operationPlus.getValue() + secondNumber + "=" + result;
                break;
            case MINUS:
                Operation operationMinus = Operation.MINUS;
                resultString = firstNumber + operationMinus.getValue() + secondNumber + "=" + result;
                break;
            case MULTIPLY:
                Operation operationMultiply = Operation.MULTIPLY;
                resultString = firstNumber + operationMultiply.getValue() + secondNumber + "=" + result;
                break;
            case DIVIDE:
                Operation operationDivide = Operation.DIVIDE;
                if (secondNumber != 0) {
                    resultString = firstNumber + operationDivide.getValue() + secondNumber + "=" + result;
                } else {
                    resultString = firstNumber + operationDivide.getValue() + secondNumber + "=" + null;
                }
                break;
            case REMAINDER:
                Operation operationRemainder = Operation.REMAINDER;
                if (secondNumber != 0) {
                    resultString = firstNumber + operationRemainder.getValue() + secondNumber + "=" + result;
                } else {
                    resultString = firstNumber + operationRemainder.getValue() + secondNumber + "=" + null;
                }
                break;
            default:
                return null;
        }
        return resultString;
    }

    public static List<String> prepareListFromFile() throws IOException {
        File fileCsv = new File("files/calculator.csv");
        List<String> operations = FileUtils.readLines(fileCsv, Charset.defaultCharset());
        List<String> list = new ArrayList<String>();

        for (String str : operations.subList(0, operations.size())) {
            String[] split = str.split(",");
            list.add(0, split[0].trim());
            list.add(1, split[1].trim());
            list.add(2, split[2].trim());
        }
        return list;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(prepareResultString(prepareListFromFile(), calculate(prepareListFromFile())));
    }
}
